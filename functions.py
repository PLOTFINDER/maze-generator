from PIL import Image
import numpy


def generateImage(board, square_size):
    squares = board.squares
    maze_data = numpy.zeros((len(squares[0]) * 2 + 1, len(squares) * 2 + 1, 3), dtype=numpy.uint8)
    new_size = len(maze_data[0]) * square_size[0], len(maze_data) * square_size[1]

    for y in range(len(maze_data)):
        for x in range(len(maze_data[0])):
            maze_data[y][x] = [0, 0, 0]

    for y in range(len(squares)):
        for x in range(len(squares[0])):
            square = squares[y][x]

            maze_data[y * 2 + 1][x * 2 + 1] = [255, 255, 255]

            if not square.walls[2]:
                maze_data[y * 2 + 1][x * 2 + 1 + 1] = [255, 255, 255]

            if not square.walls[3]:
                maze_data[y * 2 + 1 + 1][x * 2 + 1] = [255, 255, 255]

    img = Image.fromarray(maze_data)
    img = img.resize(new_size, Image.AFFINE)
    img.show()
    img.save("maze.jpeg", "jpeg")
