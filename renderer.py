import pygame


def renderBoard(window, board, walker):
    clearWindow(window)

    squares = board.squares
    square_size = board.square_size

    square_color = (255, 255, 255)
    wall_color = (255, 0, 0)

    for i in range(len(squares)):
        for j in range(len(squares[0])):
            renderSquare(window, squares[i][j], square_size, square_color)

    for i in range(len(squares)):
        for j in range(len(squares[0])):
            renderWalls(window, squares[i][j], square_size, wall_color)

    renderWalker(window, walker, square_size)


def renderSquare(window, square, square_size, square_color):
    x = square.pos.x
    y = square.pos.y

    rect = (x * square_size[0], y * square_size[1], square_size[0], square_size[1])
    pygame.draw.rect(window, square_color, rect)


def renderWalls(window, square, square_size, wall_color):
    for i, wall in enumerate(square.walls):
        if wall:

            x = square.pos.x * square_size[0]
            y = square.pos.y * square_size[1]

            if i == 0:
                start = (x, y)
                end = (x, y + square_size[1])
                pygame.draw.line(window, wall_color, start, end, 4)
            elif i == 1:
                start = (x, y)
                end = (x + square_size[0], y)
                pygame.draw.line(window, wall_color, start, end, 4)
            elif i == 2:
                start = (x + square_size[0] - 2, y)
                end = (x + square_size[0] - 2, y + square_size[1])
                pygame.draw.line(window, wall_color, start, end, 4)
            elif i == 3:
                start = (x, y + square_size[1] - 2)
                end = (x + square_size[0], y + square_size[1] - 2)
                pygame.draw.line(window, wall_color, start, end, 4)


def renderWalker(window, walker, square_size):
    color = (0, 0, 255)

    pos = (walker.pos.x * square_size[0] + square_size[0] / 2, walker.pos.y * square_size[1] + square_size[1] / 2)

    pygame.draw.circle(window, color, pos, 10)


def clearWindow(window):
    window.fill((0, 0, 0))
