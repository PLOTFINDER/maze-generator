import random

from BasicVector import Vec2


class Walker:

    def __init__(self, board_size):
        self.pos = Vec2.randomIntVec(0, board_size[0]-1, 0, board_size[1]-1)
        print(self.pos)
        self.path = []

    def update(self, board):
        result = False
        if not board.allSquaresAreVisited():
            startSquare = board.getSquare(self.pos.x, self.pos.y)

            moves = board.getNotVisitedSquares(self.pos.x, self.pos.y)
            startSquare.visited = True

            if len(moves) > 0:
                self.path.append(startSquare)
                distSquare = random.choice(moves)
                board.removeWall(startSquare, distSquare)
                self.pos = distSquare.pos
            else:
                self.path.pop()
                self.pos = self.path[-1].pos

            result = True

        return result

