from box import Box


class Board:

    def __init__(self, square_count, square_size):
        self.square_size = square_size
        self.squares = [[Box(x, y) for x in range(square_count[0])] for y in range(square_count[1])]
        self.cleanUselessWalls()

    def cleanUselessWalls(self):

        patterns = [(-1, 0), (0, -1)]

        for y in range(len(self.squares)):
            for x in range(len(self.squares[0])):
                for i, pattern in enumerate(patterns):
                    xOff = pattern[0]
                    yOff = pattern[1]
                    if self.squareIsHere(x + xOff, y + yOff):
                        self.squares[y][x].removeWall(i)

                    # print(x, y, self.squareIsHere(x + xOff, y + yOff), self.squares[y][x].walls)

    def squareIsHere(self, x, y):
        return 0 <= x < len(self.squares[0]) and 0 <= y < len(self.squares)

    def getSquare(self, x, y):
        return self.squares[y][x]

    def getNotVisitedSquares(self, x, y):
        patterns = [(-1, 0), (0, -1), (1, 0), (0, 1)]
        result = []
        for pattern in patterns:
            xOff = pattern[0]
            yOff = pattern[1]
            if self.squareIsHere(x + xOff, y + yOff):
                square = self.getSquare(x + xOff, y + yOff)
                if not square.visited:
                    result.append(square)

        return result

    def allSquaresAreVisited(self):
        result = True
        for y in range(len(self.squares)):
            for x in range(len(self.squares[0])):
                if not self.squares[y][x].visited:
                    result = False

        return result

    def removeWall(self, squareA, squareB):
        direction = squareA.pos - squareB.pos

        if direction.x == 1:
            squareB.removeWall(2)
        elif direction.x == -1:
            squareA.removeWall(2)
        elif direction.y == 1:
            squareB.removeWall(3)
        elif direction.y == -1:
            squareA.removeWall(3)


